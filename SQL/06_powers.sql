INSERT INTO "Power"(name, description)
VALUES ('Super Strength','This power grants the superhero with superhero strength (non-human quality)');
INSERT INTO "Power"(name, description)
VALUES ('Power ring','A Green Lantern Ring, also known as a Power Ring, is a piece of jewelry that grants the wearer incredible and incomprehensible powers and abilities by harnessing willpower.');
INSERT INTO "Power"(name, description)
VALUES ('Extremely wealthy','To have this power you need to be in the top 0.00000001 % of people');
INSERT INTO "Power"(name, description)
VALUES ('Strong will','Only superheros with the most extreme willpower receive this power');

INSERT INTO "superhero_power"(superhero_id, power_id) VALUES (1,3);
INSERT INTO "superhero_power"(superhero_id, power_id) VALUES (1,4);

INSERT INTO "superhero_power"(superhero_id, power_id) VALUES (2,1);
INSERT INTO "superhero_power"(superhero_id, power_id) VALUES (2,4);

INSERT INTO "superhero_power"(superhero_id, power_id) VALUES (3,2);