INSERT INTO "Superhero" (name, alias, origin) VALUES ('Bruce Wayne','Batman','Gotham City');
INSERT INTO "Superhero" (name, alias, origin) VALUES ('Clark Kent','Superman','Krypton');
INSERT INTO "Superhero" (name, alias, origin) VALUES ('Alan Scott','Green Lantern','New York');