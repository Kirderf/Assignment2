CREATE TABLE superhero_power
(
    superhero_id int REFERENCES "Superhero" On DELETE CASCADE,
    power_id int REFERENCES "Power" ON DELETE CASCADE,
    PRIMARY KEY (superhero_id, power_id)
);