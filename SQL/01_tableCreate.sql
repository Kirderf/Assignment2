DROP TABLE IF EXISTS "Assistant";
DROP TABLE IF EXISTS "superhero_power";
DROP TABLE IF EXISTS "Power";
DROP TABLE IF EXISTS "Superhero";


CREATE TABLE "Superhero"
(
    id     serial primary key,
    Name   varchar(255) NOT NULL,
    Alias  varchar(255),
    Origin varchar(255)
);
CREATE TABLE "Assistant"
(
    id   serial primary key,
    Name varchar(255) NOT NULL,
    superhero_id int REFERENCES "Superhero"
);
CREATE TABLE "Power"
(
    id          serial primary key,
    Name        varchar(255) NOT NULL,
    Description varchar(255) NOT NULL
);