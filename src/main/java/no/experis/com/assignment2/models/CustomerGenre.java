package no.experis.com.assignment2.models;
/**
 * Record representing a customer's name, their country, and the count of a specific genre associated with them.
 */
public record CustomerGenre(int id,
                            String genre,
                            int count) {
}

