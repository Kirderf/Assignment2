package no.experis.com.assignment2.models;
/**
 * Record representing a customer's name and the sum they have spent.
 */
public record CustomerSpender(int id,
                              String firstName,
                              String lastName,
                              double amount) {
}

