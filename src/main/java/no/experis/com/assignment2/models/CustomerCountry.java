package no.experis.com.assignment2.models;

/**
 * Record representing a country and the number of customers associated with that country.
 */
public record CustomerCountry(String country,
                              int count) {
}

