package no.experis.com.assignment2.repos;

import no.experis.com.assignment2.models.Customer;
import no.experis.com.assignment2.models.CustomerCountry;
import no.experis.com.assignment2.models.CustomerGenre;
import no.experis.com.assignment2.models.CustomerSpender;

import java.util.Collection;

/**
 * Interface for repository operations on the Customer entity.
 * Provides basic CRUD operations as well as some specific queries.
 */
public interface CustomerRepo extends CRUDRepository<Customer,Integer> {

    Collection<Customer> findByFirstOrLastName(String search);

    Collection<Customer> getPage(int limit, int offset);

    CustomerCountry getCountryWithMostCustomers();

    CustomerSpender getHighestSpender();

    Collection<CustomerGenre> getMostPopularGenre(int customerId);
}
