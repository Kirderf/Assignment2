package no.experis.com.assignment2.repos;

import no.experis.com.assignment2.models.Customer;
import no.experis.com.assignment2.models.CustomerCountry;
import no.experis.com.assignment2.models.CustomerGenre;
import no.experis.com.assignment2.models.CustomerSpender;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Repository;

import java.sql.*;
import java.util.Collection;
import java.util.HashSet;
import java.util.Set;

/**
 * Implementation of the CustomerRepository interface to interact with a SQL database to
 * perform CRUD operations on the 'customer' table and related queries.
 */
@Repository
public class CostumerRepoImpl implements CustomerRepo {
    private final String url;
    private final String username;
    private final String password;

    public CostumerRepoImpl(
            @Value("${spring.datasource.url}") String url,
            @Value("${spring.datasource.username}") String username,
            @Value("${spring.datasource.password}") String password) {
        this.url = url;
        this.username = username;
        this.password = password;
    }

    /**
     * Retrieves all customers from the database.
     *
     * @return a List of Customer objects.
     */
    @Override
    public Collection<Customer> findAll() {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer ";
        Set<Customer> customers = new HashSet<>();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return customers;
    }

    /**
     * Retrieves a customer by its ID.
     *
     * @param id the ID of the customer.
     * @return a Customer object or null if not found.
     */
    @Override
    public Customer findById(Integer id) {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email FROM customer WHERE customer_id = ?";
        Customer customer = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,id);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return customer;
    }

    /**
     * Retrieves a customer by its name.
     *
     * @param name the name of the customer.
     * @return a Set of customers or null if not found.
     */
    @Override
    public Collection<Customer> findByFirstOrLastName(String name) {
        String sql = "SELECT customer_id, first_name, last_name, country, postal_code, phone, email " +
                "FROM customer where first_name like ? Or last_name like ?";
        Set<Customer> customers = new HashSet<>();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1,name);
            statement.setString(2,name);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return customers;
    }

    /**
     * Retrieves a paginated list of customers.
     *
     * @param limit  the maximum number of customers to return.
     * @param offset the starting position of the first customer in the result set.
     * @return a Collection of Customer objects.
     */
    @Override
    public Collection<Customer> getPage(int limit, int offset) {
        String sql = "SELECT customer_id, first_name, last_name, company, address, city, state, country, postal_code, phone, fax, email " +
                "FROM customer offset ? limit ?";
        Set<Customer> customers = new HashSet<>();
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setInt(1,offset);
            statement.setInt(2,limit);
            ResultSet result = statement.executeQuery();
            while (result.next()) {
                Customer customer = new Customer(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getString("country"),
                        result.getString("postal_code"),
                        result.getString("phone"),
                        result.getString("email")
                );
                customers.add(customer);
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return customers;
    }

    /**
     * Inserts a new customer into the database.
     *
     * @param object the Customer object to be added.
     * @return the number of affected rows in the database.
     */
    @Override
    public int insert(Customer object) {
        String sql = "INSERT INTO customer (first_name, last_name, country, postal_code, phone, email) VALUES (?, ?, ?, ?, ?, ?)";
        int result = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);

            statement.setString(1, object.firstName());
            statement.setString(2, object.lastName());
            statement.setString(3, object.country());
            statement.setString(4, object.postalCode());
            statement.setString(5, object.phoneNumber());
            statement.setString(6, object.email());

            result = statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return result;
    }

    /**
     * Updates the details of an existing customer in the database.
     *
     * @param object the Customer object with updated details.
     * @return the number of affected rows in the database.
     */
    @Override
    public int update(Customer object) {
        String sql = "UPDATE customer SET first_name=?, last_name=?, country=?, postal_code=?, phone=?, email=? WHERE customer_id=?";
        int result = 0;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            statement.setString(1, object.firstName());
            statement.setString(2, object.lastName());
            statement.setString(3, object.country());
            statement.setString(4, object.postalCode());
            statement.setString(5, object.phoneNumber());
            statement.setString(6, object.email());
            statement.setInt(7,object.id());
            result = statement.executeUpdate();
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return result;
    }


    /**
     * Retrieves the country with the most customers.
     *
     * @return a CustomerCountry object representing the country with the highest number of customers.
     */
    @Override
    public CustomerCountry getCountryWithMostCustomers() {
        String sql = "SELECT country,count(country) As count from customer GROUP BY country ORDER BY count DESC LIMIT 1";
        CustomerCountry customer = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                customer = new CustomerCountry(
                        result.getString("country"),
                        result.getInt("count")
                );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return customer;
    }

    /**
     * Retrieves the customer who has the highest spending in the database.
     *
     * @return a CustomerSpender object representing the customer with the highest spending.
     */
    @Override
    public CustomerSpender getHighestSpender() {
        String sql = "SELECT customer.customer_id,first_name,last_name,Sum(total) as sum from customer join invoice i on customer.customer_id = i.customer_id GROUP BY customer.customer_id ORDER BY sum DESC LIMIT 1";
        CustomerSpender customer = null;
        try (Connection conn = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = conn.prepareStatement(sql);
            ResultSet result = statement.executeQuery();
            if (result.next()) {
                customer = new CustomerSpender(
                        result.getInt("customer_id"),
                        result.getString("first_name"),
                        result.getString("last_name"),
                        result.getDouble("sum")
                );
            }
        } catch (SQLException e) {
            System.out.println(e.getMessage());
        }
        return customer;
    }

    /**
     * Retrieves the most popular genre for a specific user.
     *
     * @param customerId the ID of the user.
     * @return a CustomerGenre object representing the most popular genre for the given user.
     */
    @Override
    public Collection<CustomerGenre> getMostPopularGenre(int customerId) {
        Set<CustomerGenre> customerGenres = new HashSet<>();
        String sql = "SELECT customer_id, genre.name, count(*) as genre_count" +
                " FROM customer" +
                " INNER JOIN invoice USING (customer_id)" +
                " INNER JOIN invoice_line USING (invoice_id)" +
                " INNER JOIN track USING (track_id)" +
                " INNER JOIN genre USING (genre_id)" +
                " WHERE customer_id = ?" +
                " GROUP BY customer_id,genre.name" +
                " having" +
                " count(*) = (" +
                " select max(genre_count)" +
                " from (" +
                " select genre.name as genre_name, count(*) as genre_count" +
                " from customer" +
                " INNER JOIN invoice USING (customer_id)" +
                " INNER JOIN invoice_line USING (invoice_id)" +
                " INNER JOIN track USING (track_id)" +
                " INNER JOIN genre USING (genre_id)" +
                " where customer_id = ?" +
                " group by genre.name" +
                ") as tab" +
                ")" +
                " ORDER BY genre_count DESC";
        try (Connection connection = DriverManager.getConnection(url, username, password)) {
            PreparedStatement statement = connection.prepareStatement(sql);

            statement.setInt(1, customerId);
            statement.setInt(2, customerId);

            ResultSet result = statement.executeQuery();

            while (result.next()) {
                CustomerGenre customerGenre = new CustomerGenre(
                        result.getInt(1),
                        result.getString(2),
                        result.getInt(3));

                customerGenres.add(customerGenre);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return customerGenres;
    }
}
