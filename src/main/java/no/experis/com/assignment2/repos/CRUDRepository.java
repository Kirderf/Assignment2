package no.experis.com.assignment2.repos;

import java.util.Collection;

/**
 * A generic interface for CRUD (Create, Read, Update, Delete) operations.
 * Provides basic operations on entities of type {@code T} with an ID of type {@code U}.
 *
 * @param <T> The type of the entity.
 * @param <U> The type of the entity's identifier.
 *
 */
public interface CRUDRepository<T, U> {
    Collection<T> findAll();

    T findById(U id);

    int insert(T object);

    int update(T object);

}
