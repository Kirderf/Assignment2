package no.experis.com.assignment2.runners;

import no.experis.com.assignment2.models.Customer;
import no.experis.com.assignment2.models.CustomerGenre;
import no.experis.com.assignment2.repos.CustomerRepo;
import org.springframework.boot.ApplicationArguments;
import org.springframework.boot.ApplicationRunner;
import org.springframework.stereotype.Component;

import java.util.Collection;

@Component
public class PgAppRunner implements ApplicationRunner {

    final CustomerRepo CustomerRepo;

    public PgAppRunner(CustomerRepo CustomerRepo) {
        this.CustomerRepo = CustomerRepo;
    }

    @Override
    public void run(ApplicationArguments args) {

        System.out.println("Get all customers");
        System.out.println("------------------");
        Collection<Customer> customer = CustomerRepo.findAll();
        for (Customer value : customer) {
            System.out.println(value);
        }


        System.out.println("------------------");
        System.out.println("Get customer by Id: ");

        Customer getCustomerById = CustomerRepo.findById(2);
        System.out.println(getCustomerById);

        System.out.println("------------------");
        System.out.println("Get customer by name: ");

        Collection<Customer> getCustomerByName = CustomerRepo.findByFirstOrLastName("Helena");
        for (Customer customer1 : getCustomerByName) {
            System.out.println(customer1);
        }

        System.out.println("---------------");
        System.out.println("Customer pages");
        System.out.println(" ");

        Collection<Customer> customerPage = CustomerRepo.getPage(10, 0);
        for (Customer customer1 : customerPage) {
            System.out.println(customer1);
        }

        int customerSize = CustomerRepo.findAll().size();

        Customer newCustomer = new Customer(customerSize + 1, "Fredrik", "Julsen", "Norway", "7047", "95820408", "fredrik.julsen@no.experis.com");
        System.out.println("Insert : " + CustomerRepo.insert(newCustomer));
        System.out.println("--------------");
        System.out.println("New Customer:");
        System.out.println(" ");

        Collection<Customer> getNewCustomerByName = CustomerRepo.findByFirstOrLastName("Fredrik");
        for (Customer customer1 : getNewCustomerByName) {
            System.out.println(customer1);
        }


        System.out.println("--------------");
        System.out.println("Update Customer:");
        System.out.println(" ");


        Customer updateCustomer = new Customer(newCustomer.id(), newCustomer.firstName(), newCustomer.lastName(), "Asia", newCustomer.postalCode(), newCustomer.phoneNumber(), newCustomer.email());

        System.out.println("Rows affected " + CustomerRepo.update(updateCustomer));
        System.out.println(updateCustomer);


        System.out.println("--------------");
        System.out.println("Get Country with most Customers:");

        System.out.println(CustomerRepo.getCountryWithMostCustomers());

        System.out.println("--------------");
        System.out.println("Get highest spending customer:");

        System.out.println(CustomerRepo.getHighestSpender());

        System.out.println("--------------");
        System.out.println("Get the most popular genre for given customer:");

        for (CustomerGenre customerGenre : CustomerRepo.getMostPopularGenre(12)) {
            System.out.println(customerGenre);
        }

    }

}
